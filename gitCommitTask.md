## Check point 1: Ensure that you switch branch from master to feature-111:
commit to the feature-111 branch after "git checkout feature-111"  not to the master branch.

## Check point 2: feature chnages in userAuth api:
commit message: Modify the userAuth api to change the token validity to 24 hours instead of 48 hours.


## Check point 3: Resetting the users:
commit message: Resetting all the users whose account was created on or after 19/07/2020 and all the tokens they have recieved during this period is not valid anymore.


## Check point 4: Inform the user of the changes done: 
commit message: Added an alert on to the login page to indicate the invalidation of tokens.

## Check point 5: User datails form creation: 
commit message: Created an registration form in the login page which will take users mobile number as input.


## Check point 6: Country authentication logic: 
commit message: Added an authentication logic to the input field of country code section to verify if the users mobile number belongs to India or UK. If not registration will be denied.

## Check point 7: Welcome and confirmation email: 
commit message: Added a logic in the backend userSignup.js file to trigger a welcome message as well as a confirmation message on successful signup. This message will be sent to the user's email as a confirmation of successful signup.

## Check point 8: Merge request:
Create a merge request to the master branch.
